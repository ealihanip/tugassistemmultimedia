﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Class Form1
    Public hisR(0 To 255) As Decimal
    Public hisG(0 To 255) As Decimal
    Public hisB(0 To 255) As Decimal

    Public hmaxr As Decimal = 0
    Public hmaxg As Decimal = 0
    Public hmaxb As Decimal = 0

    Public hisR2(0 To 255) As Decimal
    Public hisG2(0 To 255) As Decimal
    Public hisB2(0 To 255) As Decimal

    Public hmaxr2 As Decimal = 0
    Public hmaxg2 As Decimal = 0
    Public hmaxb2 As Decimal = 0
    Public kesamaanR As Decimal = 0
    Public kesamaanG As Decimal = 0
    Public kesamaanB As Decimal = 0

    Private Sub carikesamaan()

        kesamaanG = 0
        kesamaanB = 0
        kesamaanR = 0


        For i As Integer = 0 To 255
            If hisR(i) = hisR2(i) Then
                kesamaanR = 1
                Label11.Text = "Merah Sama"
            Else : Label11.Text = "Merah TIdak Sama"

            End If
            If hisR(i) = hisR2(i) Then
                Label12.Text = "Hijau Sama"
                kesamaanG = 1
            Else : Label12.Text = "Hijau Tidak Sama"
            End If
            If hisR(i) = hisR2(i) Then
                Label13.Text = "Biru Sama"
                kesamaanB = 1
            Else : Label13.Text = "Biru Tidak Sama"
            End If
            If kesamaanR = 1 And kesamaanG = 1 And kesamaanB = 1 Then
                Label15.Text = "Gambar Sama"
            Else
                Label15.Text = "Gambar Tidak Sama"
            End If
        Next
        
        

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim filename As String
        With Me.pilihgambar1 'untuk mengatur properti pada picture Box
            .Filter = "JPG(*.jpg)|*.jpg" ' jenis file yang ‘ditampilkan
            .FilterIndex = 2  'mengatur jenis file default
            .InitialDirectory = "D:\" ' inisial direktori
            .Multiselect = False ' untuk mengatur boleh tidaknya pengambilan ‘lebih dari 1 file
            .DefaultExt = "jpg"    ' ekstensi default
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = .FileName
            End If

            If Not (pilihgambar1 Is Nothing) Then
                'mengatur mode tampilan picturebox
                Me.PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
                Me.PictureBox1.Image = Bitmap.FromFile(filename)   'pengambilan gambar untuk ditampilkan
            End If
        End With


        Call proseshistogram()
        For max As Integer = 0 To 254
            If hisR(max) < hisR(max + 1) Then
                Label4.Text = hisR(max + 1) + (hisR(max + 1) / 2)
            End If
            If hisG(max) < hisG(max + 1) Then
                Label5.Text = hisG(max + 1) + (hisG(max + 1) / 2)
            End If
            If hisB(max) < hisB(max + 1) Then
                Label6.Text = hisB(max + 1) + (hisB(max + 1) / 2)
            End If

        Next
        
    End Sub


    Private Sub proseshistogram()

        Dim gambar As New Bitmap(PictureBox1.Image)

        For i As Integer = 0 To 255
            hmaxr = 0
            hmaxg = 0
            hmaxb = 0
            hisR(i) = 0
            hisG(i) = 0
            hisB(i) = 0
        Next

        For j As Integer = 0 To gambar.Width - 1
            For k As Integer = 0 To gambar.Height - 1
                Dim merah As Integer = gambar.GetPixel(j, k).R
                Dim hijau As Integer = gambar.GetPixel(j, k).G
                Dim biru As Integer = gambar.GetPixel(j, k).B


                hisR(merah) = hisR(merah) + 1
                hisG(hijau) = hisG(hijau) + 1
                hisB(biru) = hisB(biru) + 1
            Next
        Next

        For m As Integer = 0 To 255
            hisR(m) = hisR(m) / ((gambar.Width) * (gambar.Height))
            hisG(m) = hisG(m) / ((gambar.Width) * (gambar.Height))
            hisB(m) = hisB(m) / ((gambar.Width) * (gambar.Height))
            If hisR(m) > hmaxr Then
                hmaxr = hisR(m)

            End If
            If hisG(m) > hmaxg Then
                hmaxg = hisG(m)
            End If
            If hisB(m) > hmaxb Then
                hmaxb = hisB(m)
            End If

        Next
        PictureBox2.Invalidate()
        PictureBox3.Invalidate()
        PictureBox4.Invalidate()

    End Sub

    
    Private Sub PictureBox2_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox2.Paint

        If hmaxr <= 0 Then Exit Sub
        Dim h As Integer = PictureBox2.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisR(i) / (hmaxr + (hmaxr / 2)))
            Dim x As Decimal = PictureBox2.Width * i / 256

            Using pena As New Pen(Color.Red, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub

    Private Sub PictureBox3_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox3.Paint

        If hmaxg <= 0 Then Exit Sub
        Dim h As Integer = PictureBox3.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisG(i) / (hmaxg + (hmaxg / 2)))
            Dim x As Decimal = PictureBox3.Width * i / 256

            Using pena As New Pen(Color.Green, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub
    Private Sub PictureBox4_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox4.Paint

        If hmaxb <= 0 Then Exit Sub
        Dim h As Integer = PictureBox4.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisB(i) / (hmaxb + (hmaxb / 2)))
            Dim x As Decimal = PictureBox4.Width * i / 256

            Using pena As New Pen(Color.Blue, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim filename As String
        With Me.pilihgambar2 'untuk mengatur properti pada picture Box
            .Filter = "JPG(*.jpg)|*.jpg" ' jenis file yang ‘ditampilkan
            .FilterIndex = 2  'mengatur jenis file default
            .InitialDirectory = "D:\" ' inisial direktori
            .Multiselect = False ' untuk mengatur boleh tidaknya pengambilan ‘lebih dari 1 file
            .DefaultExt = "jpg"    ' ekstensi default
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = .FileName
            End If

            If Not (pilihgambar2 Is Nothing) Then
                'mengatur mode tampilan picturebox
                Me.PictureBox5.SizeMode = PictureBoxSizeMode.StretchImage
                Me.PictureBox5.Image = Bitmap.FromFile(filename)   'pengambilan gambar untuk ditampilkan
            End If
        End With


        Call proseshistogram2()
        For max As Integer = 0 To 254
            If hisR2(max) < hisR2(max + 1) Then
                Label7.Text = hisR2(max + 1) + (hisR2(max + 1) / 2)
            End If
            If hisG2(max) < hisG2(max + 1) Then
                Label8.Text = hisG2(max + 1) + (hisG2(max + 1) / 2)
            End If
            If hisB2(max) < hisB2(max + 1) Then
                Label9.Text = hisB2(max + 1) + (hisB2(max + 1) / 2)
            End If

        Next

        Call carikesamaan()

    End Sub


    Private Sub proseshistogram2()

        Dim gambar As New Bitmap(PictureBox5.Image)

        For i As Integer = 0 To 255
            hmaxr2 = 0
            hmaxg2 = 0
            hmaxb2 = 0
            hisR2(i) = 0
            hisG2(i) = 0
            hisB2(i) = 0
        Next

        For j As Integer = 0 To gambar.Width - 1
            For k As Integer = 0 To gambar.Height - 1
                Dim merah As Integer = gambar.GetPixel(j, k).R
                Dim hijau As Integer = gambar.GetPixel(j, k).G
                Dim biru As Integer = gambar.GetPixel(j, k).B


                hisR2(merah) = hisR2(merah) + 1
                hisG2(hijau) = hisG2(hijau) + 1
                hisB2(biru) = hisB2(biru) + 1
            Next
        Next

        For m As Integer = 0 To 255
            hisR2(m) = hisR2(m) / ((gambar.Width) * (gambar.Height))
            hisG2(m) = hisG2(m) / ((gambar.Width) * (gambar.Height))
            hisB2(m) = hisB2(m) / ((gambar.Width) * (gambar.Height))
            If hisR2(m) > hmaxr2 Then
                hmaxr2 = hisR2(m)
            End If
            If hisG2(m) > hmaxg2 Then
                hmaxg2 = hisG2(m)
            End If
            If hisB2(m) > hmaxb2 Then
                hmaxb2 = hisB2(m)
            End If

        Next
        PictureBox6.Invalidate()
        PictureBox7.Invalidate()
        PictureBox8.Invalidate()

    End Sub
    Private Sub PictureBox6_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox6.Paint

        If hmaxr2 <= 0 Then Exit Sub
        Dim h As Integer = PictureBox6.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisR2(i) / (hmaxr2 + (hmaxr2 / 2)))
            Dim x As Decimal = PictureBox6.Width * i / 256

            Using pena As New Pen(Color.Red, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub

    Private Sub PictureBox7_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox7.Paint

        If hmaxg2 <= 0 Then Exit Sub
        Dim h As Integer = PictureBox7.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisG2(i) / (hmaxg2 + (hmaxg2 / 2)))
            Dim x As Decimal = PictureBox7.Width * i / 256

            Using pena As New Pen(Color.Green, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub
    Private Sub PictureBox8_Paint(sender As Object, e As PaintEventArgs) Handles PictureBox8.Paint

        If hmaxb2 <= 0 Then Exit Sub
        Dim h As Integer = PictureBox8.Height

        For i As Integer = 0 To 255
            Dim tmax As Decimal = CInt(h * hisB2(i) / (hmaxb2 + (hmaxb2 / 2)))
            Dim x As Decimal = PictureBox4.Width * i / 256

            Using pena As New Pen(Color.Blue, 2)
                e.Graphics.DrawLine(pena, x, h - tmax, x, h)
            End Using
        Next

    End Sub

   
End Class
